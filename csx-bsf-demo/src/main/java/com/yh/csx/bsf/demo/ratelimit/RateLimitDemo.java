package com.yh.csx.bsf.demo.ratelimit;

import com.google.common.util.concurrent.RateLimiter;
import com.yh.csx.bsf.cat.CatProperties;
import com.yh.csx.bsf.cat.CatRateLimiter;
import com.yh.csx.bsf.cat.util.UrlParser;

import java.util.HashMap;
import java.util.Map;

/**
 * @创建人 霍钧城
 * @创建时间 2021年06月18日 14:50:00
 * @描述
 */
public class RateLimitDemo {
    public static void main(String[] args) {
        String type="url";
        String requestURI="/rateLimit/demo";
        String resource= CatProperties.getRateLimiterCatName()+"-"+type+"-"+ UrlParser.format(requestURI);
        Map map=new HashMap();
        map.put(resource,RateLimiter.create(1.7d).getRate());
        CatRateLimiter.updateResourceQpsAll(map);
        for (int i = 0; i <10; i++) {
            CatRateLimiter.enter(type,requestURI);
        }
    }
}
