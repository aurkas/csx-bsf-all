package com.yh.csx.bsf.demo.file;

import com.yh.csx.bsf.core.util.WebUtils;
import com.yh.csx.bsf.file.FileProvider;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * @author Huang Zhaoping
 */
@RestController
@SpringBootApplication
@ComponentScan("com.yh.csx.bsf.demo.config")
public class FileApplication {

    @Autowired(required = false)
    private FileProvider fileProvider;

    @ApiOperation("上传图片文件路径")
    @PostMapping("/uploadProcessImage")
    public String uploadProcessImageByPath(MultipartFile file) throws Exception {
        String filePath = WebUtils.getRequest().getServletContext().getRealPath("/")+file.getOriginalFilename();
        file.transferTo(new File(filePath));
        return fileProvider.uploadProcessImage(filePath,true);
    }

    @ApiOperation("上传文件路径")
    @PostMapping("/uploadFile")
    public String uploadFileByPath(MultipartFile file) throws Exception {
        String filePath = WebUtils.getRequest().getServletContext().getRealPath("/")+file.getOriginalFilename();
        file.transferTo(new File(filePath));
        String url=fileProvider.uploadFile(filePath,false);
        return url;
    }
    public static void main(String[] args) {
        SpringApplication.run(FileApplication.class, args);
    }
}
