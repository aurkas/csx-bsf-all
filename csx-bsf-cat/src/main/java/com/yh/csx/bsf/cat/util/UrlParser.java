package com.yh.csx.bsf.cat.util;

import java.util.regex.Pattern;

public class UrlParser {
    public static final char SPLIT = '/';
    public static String[] billNo = new String[]{"AP", "APU", "BS", "C", "CA", "CB", "CDZ", "CP", "CPH", "CR", "CY", "DB", "DZC", "DZG",
			"FF", "FH", "FL", "FN", "FP", "FY", "GD", "GPC", "GPG", "GX", "GXX", "H", "HD", "HX", "HZ", "IN", "JB", "JCXM", "JCXM000002",
			"JCXM000003", "JCXM000004", "JCXM000005", "JCXM000006", "JCXM000007", "JCXM000008", "JCXM000009", "JCXM000010", "JCXM000011",
			"JCXM000012", "JCXM000013", "JCXM000014", "JCXM000015", "JCXM000016", "JCXM000017", "JCXM000018", "JCXM000019", "JCXM000020",
			"JCXM000021", "JCXM000022", "JCXM000023", "JCXM000024", "JCXM000025", "JCXM000026", "JCXM000027", "JCXM000028", "JCXM000029",
			"JCXM000030", "JH", "JP", "JS", "KP", "KS", "LP", "LY", "MC", "MR", "MV", "OC", "OD", "ODZ", "OH", "OM", "OS", "OT", "OU", "OW"
			, "OY", "P", "PA", "PAY", "PD", "PO", "PP", "PR", "PZ", "R1", "RH", "RM", "RO", "RP", "RR", "RT", "SBJ", "SC", "SDZ", "SH",
			"SI", "SJ", "SS", "SY", "SZ", "T", "TC", "TD", "TH", "TK", "TO", "TS", "WC", "WD", "WDZC", "WDZG", "WG", "WO", "WV", "XK", "XP"
			, "YC", "YDZC", "YDZG", "YG", "YHCSX-SA-AH", "YHCSX-SA-AH-TC", "YHCSX-SA-CD", "YHCSX-SA-CD-TC", "YHCSX-SA-CQ", "YHCSX-SA-CQ-TC"
			, "YHCSX-SA-FZ", "YHCSX-SA-FZ-TC", "YHCSX-SA-KS", "YHCSX-SA-KS-TC", "YHCSX-SA-NB-TC", "YHCSX-SA-NJ", "YHCSX-SA-NJ-TC", "YHCSX" +
			"-SA-NP", "YHCSX-SA-NP-TC", "YHCSX-SA-PT", "YHCSX-SA-PT-TC", "YHCSX-SA-PZH", "YHCSX-SA-PZH-TC", "YHCSX-SA-QJ-TC", "YHCSX-SA-SH"
			, "YHCSX-SA-SH-TC", "YHCSX-SA-SJZ", "YHCSX-SA-SJZ-TC", "YHCSX-SA-WZ", "YHCSX-SA-WZ-TC", "YHCSX-SA-XA", "YHCSX-SA-XA-TC",
			"YHCSX-SA-ZJ-TC", "Z", "Z1", "ZB", "ZC", "ZH", "ZM"};
    public static Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");

    public static String format(String url) {
        int length = url.length();
        StringBuilder sb = new StringBuilder(length);

        for (int index = 0; index < length; ) {
            char c = url.charAt(index);

            if (c == SPLIT && index < length - 1) {
                sb.append(c);

                StringBuilder nextSection = new StringBuilder();
                boolean isNumber = false;
                boolean first = true;

                for (int j = index + 1; j < length; j++) {
                    char next = url.charAt(j);

                    if ((first || isNumber == true) && next != SPLIT) {
                        isNumber = isNumber(next);
                        first = false;
                    }

                    if (next == SPLIT) {
                        if (isNumber) {
                            sb.append("{num}");
                        } else {
                            sb.append(isBillNo(nextSection.toString()));
                        }
                        index = j;

                        break;
                    } else if (j == length - 1) {
                        if (isNumber) {
                            sb.append("{num}");
                        } else {
                            nextSection.append(next);
                            sb.append(isBillNo(nextSection.toString()));
                        }
                        index = j + 1;
                        break;
                    } else {
                        nextSection.append(next);
                    }
                }
            } else {
                sb.append(c);
                index++;
            }
        }

        return sb.toString();
    }

    public static String isBillNo(String str) {
        for (String bill : billNo) {
            if (str.indexOf(bill) >= 0) {
                if (isInteger(str.replace(bill, ""))) {
                    str = "{billNo}";
                    break;
                }
            }
        }
        return str;
    }

    public static boolean isInteger(String str) {
        return pattern.matcher(str).matches();
    }

    public static boolean isNumber(char c) {
        return (c >= '0' && c <= '9') || c == '.' || c == '-' || c == ',';
    }
}
