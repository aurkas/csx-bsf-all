package com.yh.csx.bsf.cat;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
/**
 * @author: chejiangyi
 * @version: 2019-08-12 11:12
 **/
@ConfigurationProperties
public class CatProperties {
    @Value("${cat.server.url:}")
    @Getter
    private String catServerUrl;
    public static String Prefix="cat.";
    public static String Project="Cat";
    public static String SpringApplicationName = "spring.application.name";
    public static String UserDir="user.dir";
    public static String CatHome="CAT_HOME";
    public static String CatServerUrl="cat.server.url";
    public static String CatEnabled="bsf.cat.enabled";

    public static String RateLimiterEnabled = "bsf.rateLimiter.enabled";
    public static String RateLimiterLoggerEnabled = "bsf.rateLimiter.logger.enabled";
    public static String RateLimiterRedisTimes = "bsf.rateLimiter.redis.times";
    @Getter@Setter
    public static String  RateLimiterCatName;
}
